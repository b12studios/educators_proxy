<?php 
// Load up the Basic LTI Support code
require_once 'ims-blti/blti.php';

// Initialize, all secrets are 'secret', do not set session, and do not redirect
$deetsarray = Array (
	mysql_server   => "localhost",
	mysql_db       => "lti",
	mysql_user     => "sqladmin",
	mysql_pass     => "RandomStuffMatters",
	table	       => "blti_keys",
);

$context = new BLTI($deetsarray, false, false);

if ( !$context->valid ) {
  error_log("BLTI FAILURE: ".$context->message);
  die("Invalid Request, Contact support for further assistance.");
}

?>
if ( $context->valid ) {
    print "<pre>\n";
    print "Context Information:\n\n";
    print $context->dump();
    print "</pre>\n";
} else {
    print "<p style=\"color:red\">Could not establish context: ".$context->message."<p>\n";
}

print "<pre>\n";
print "Raw POST Parameters:\n\n";
foreach($_POST as $key => $value ) {
    print "$key=$value\n";
}
print "</pre>";

?>
