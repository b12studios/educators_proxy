<?PHP

if (!isset($_GET['hash']) || $_GET['hash'] == '' ) {
  die("Required data not found");
}

// Do we have a valid one time login hash

       	$pdo = new PDO('mysql:host=localhost;dbname=badhacks', 'badhacks', 'zomgbadhacks');
        $query = "SELECT * from cppassthroughlogins where hash='".$_GET['hash']."'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $mysqldata = $stmt->fetchAll();

        if (!isset($mysqldata[0])) {
		die("Required data invalid.");
	}

$mysqldata = $mysqldata[0];

	$query = "DELETE from cppassthroughlogins where hash='".$_GET['hash']."'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();

// Looks like we do, Lets create a login session on the box.

# Lookup Hash for User
$whmusername = "wgueducatorsvega";

# The contents of /root/.accesshash
$whmhash = "40bfdc97ca48d8f81004c69eeadfbc65
1c141ed13d47745eb3122757306095d8
5afe9beecb2442be3bbac304a2ff3b1a
f82ad987210c0f846b5a8f32c58fe708
e59eea6d9743a3afe84e1f56afc8ec14
c62fd27edab6a4f83976fc84b4bebb1d
305d9456326dc8344c94075d8b6a8f60
2997f6eed74bd9c15efaf50e4fb09088
afbcc1f34c6df9101775ff0ac041f64b
642b65216bf6fff99fedc27754fa9e06
ab365194cdffb05e67660ee7fe0fcfa4
067c86ea8d7a4d64a6613097861dba82
e45700c529f98f21c4f498d4d6089e11
f1c1e6f42b81f3af3a070dfe32ce6630
42efcb53949349860e6a4ee4d9c11548
0e24c3b4c01c67f4db1523c8b08fb751
03340e71ccb77257e544a1b397c7071f
90935ac894a35e74e5ab4c18119e4a96
fa05339e43cc42489a0295c2d89fa358
4b485dc8e24a528d98000b41d96d5033
2b92824dc4ca36e9de4750855462692c
dc5eedb70df1e867de528f8c98a50a35
271b52fff778640815cbc186026acc71
25d9b4010abd2739a344329815295ee4
324c610091197e7534886be5e9d53f06
622eb7a2f767fca1eca6e7ee534ee10d
e55feef4fa2a6dda95e7107d454723f7
d8189c715919979979b71f7b697c7d61
b2f9c74b7b5ba3b6b6ec1958ec947b7d";

$query = "https://educators.servers.mystudenthosting.com:2087/json-api/create_user_session?api.version=1&user=".$mysqldata['username']."&service=cpaneld";

$curl = curl_init();
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);

$header[0] = "Authorization: WHM $whmusername:" . preg_replace("'(\r|\n)'","",$whmhash);
curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
curl_setopt($curl, CURLOPT_URL, $query);

$result = curl_exec($curl);
if ($result == false) {
    error_log("[ProxyPassThrough] CreateUserSession: curl_exec threw error \"" . curl_error($curl) . "\" for $query");
    die("Passthrough Login Error #018, Please retry your login.");
}
curl_close($curl);

$json = json_decode($result,true);

if ( !$json ) {
  error_log("[ProxyPassThrough] CreateUserSession: Could not decode json return, Here is output. \"" . $result . "\"");
  die("Passthrough Login Error #025, Please Retry your login.");
}

// We have created a session, Now Login.
$url = preg_replace('/^http[s]:\/\/[^:]+:[0-9]+/i','http://educators.instance.mystudenthosting.com',$json['data']['url']);
error_log("URL: ".$url);

$ch = curl_init();
curl_setopt($ch, CURLOPT_VERBOSE, true);
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HEADER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);

$html = curl_exec($ch);

if ($html == false) {
    error_log("ProxyPassthrough: curl_exec threw error \"" . curl_error($ch) . "\" for $query");
    die("Passthough Login Error #019, Please Retry your login.");
}

$headersplit = explode("\r\n",$html);

foreach ($headersplit as $line) {
  preg_match('/^Set-Cookie: (.*)/',$line,$cookie);
  if ( $cookie ) {
    Header($line,false);
    continue;
  }
  if ( preg_match('/^Location: \/cpsess([0-9]+)/',$line,$location) ) {
    $cpsession = $location[1];
  }
}

if (!isset($cpsession) || $cpsession == "" ) {
  error_log("[ProxyPassthrough] Could not get CPSession in CPLoginResponse, HTML from Response Follows: \"".$html."\"");
  die("Passthough Login Error #097, Please Retry your login.");
}

Header("HTTP/1.0 302 Moved");
Header("Location: http://panel.mystudenthosting.com/cpreturn?session=".$cpsession);
