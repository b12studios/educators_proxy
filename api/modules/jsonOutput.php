<?php
    
    class jsonOutput {
        
        function __construct(&$parentObj) {
            $this->moduledata = Array(
                                'version' => '0.01',
                                'dependencies' => 'waffles',
                                );
            $this->parent = &$parentObj;
            $class = get_class();
            $this->moduledata['class'] = $class;
        }

        private function sendResponseJson($httpstatus,$resultnum,$reason,$data = "") {
            if ( !is_object($data) && !is_array($data) && $data !== "" ) {
                $encode = json_decode($data);
                if ( !$encode ) {
                    return(Array('result' => '0', 'reason' => "data variable supplied to sendResponseJson of type ".gettype($data)." is not supported, Supported types are json, object, and array."));
                }
            } elseif ( is_object($data) ) {
                $data = (array) $data;
            }
            switch ($httpstatus) {
                case "200":
                    header("HTTP/1.0 200 OK");
                    break;
                case "400":
                    header("HTTP/1.0 400 Bad Request");
                    break;
                case "401":
                    header("HTTP/1.0 401 Unauthorized");
                    break;
                case "403":
                    header("HTTP/1.0 403 Forbidden");
                    break;
                case "404":
                    header("HTTP/1.0 404 Not Found");
                    break;
                case "409":
                    header("HTTP/1.0 409 Conflict");
                    break;
                case "418":
                    header("HTTP/1.0 418 I'm a teapot");
                    break;
                case "500":
                    header("HTTP/1.0 500 Internal Server Error");
                    break;
                case "501":
                    header("HTTP/1.0 501 Not Implemented");
                    break;
                case "503":
                    header("HTTP/1.0 503 Service Unavailable");
                    break;
                default:
                    return(Array('result' => '0', 'reason' => "httpstatus supplied to sendResponseJson of value \"$httpstatus\" is not supported"));
                    break;
            }
            echo json_encode(Array('result' => (string) $resultnum, 'reason' => $reason, 'data' => $data));
            return(Array('result' => '1', 'reason' => 'Success'));
        }
        
        public function sendResponse_Ok($data, $reason = "Success") {
            return($this->sendResponseJson("200","1",$reason,$data));
        }
        
        public function sendResponse_GenFail($reason) {
            return($this->sendResponseJson("200","0",$reason));
        }
        
        public function sendResponse_BadRequest($reason) {
            return($this->sendResponseJson("400","501","Bad Request: ".$reason));
        }
        
        public function sendResponse_NotAllowed($reason) {
            return($this->sendResponseJson("403","2","Not Allowed: ".$reason));
        }
        
        public function sendResponse_NotFound($reason) {
            return($this->sendResponseJson("404","3",$reason));
        }
        
        public function sendResponse_Collision($reason) {
            return($this->sendResponseJson("409","4",$reason));
        }
        
        public function sendResponse_APIError($reason) {
            return($this->sendResponseJson("500","500","API Error: ".$reason));
        }
        
        public function sendResponse_NotImplemented($reason = "Method or Request not implemented") {
            return($this->sendResponseJson("501","5",$reason));
        }
        
        public function sendResponse_LoginRequired($reason = "Login Required!") {
            return($this->sendResponseJson("403","502",$reason));
        }
    }
