<?php
    
    class cPanelAPI {
        
        function __construct($parentObj) {
            $this->moduledata = Array(
                                      'version' => '0.01',
                                      'dependencies' => Array('db'),
                                      'requires_init' => "setup",
                                      );
            $this->parent = &$parentObj;
            $class = get_class();
            $this->moduledata['class'] = $class;
        }
        
        public function listAccounts() {
            $return = '{"data":{"acct":[{"maxaddons":"*unknown*","ip":"educators.servers.mystudenthosting.com","min_defer_fail_to_trigger_protection":"5","legacy_backup":0,"diskused":"none","maxftp":"unlimited","startdate":"14 Oct 19 07:46","max_defer_fail_percentage":"unlimited","disklimit":"unlimited","is_locked":0,"suspendtime":null,"email":"thunsaker86@gmail.com","domain":"timh1986.wgu.vegadigitalsolutions.com","unix_startdate":1413690362,"user":"timh1986","plan":"wgueducatorsvega_Student","shell":"/usr/local/cpanel/bin/noshell","maxpop":"unlimited","backup":0,"theme":"paper_lantern","owner":"wgueducatorsvega","max_email_per_hour":"unlimited","ipv6":[],"suspendreason":"not suspended","maxlst":"unlimited","suspended":0,"maxsql":"unlimited","maxparked":"*unknown*","partition":"home","maxsub":"unlimited"},{"maxaddons":"*unknown*","ip":"educators.servers.mystudenthosting.com","min_defer_fail_to_trigger_protection":"5","legacy_backup":0,"diskused":"none","maxftp":"unlimited","startdate":"14 Oct 20 10:16","max_defer_fail_percentage":"unlimited","disklimit":"unlimited","is_locked":0,"suspendtime":null,"email":"thunsaker86@gmail.com","domain":"imastudent.mystudenthosting.com","unix_startdate":1413785818,"user":"imastudent","plan":"default_imastudent","shell":"/usr/local/cpanel/bin/noshell","maxpop":"unlimited","backup":0,"theme":"paper_lantern","owner":"wgueducatorsvega","max_email_per_hour":"unlimited","ipv6":[],"suspendreason":"not suspended","maxlst":"unlimited","suspended":0,"maxsql":"unlimited","maxparked":"*unknown*","partition":"home","maxsub":"unlimited"},{"maxaddons":"*unknown*","ip":"educators.servers.mystudenthosting.com","min_defer_fail_to_trigger_protection":"5","legacy_backup":0,"diskused":"none","maxftp":"unlimited","startdate":"14 Nov 02 02:34","max_defer_fail_percentage":"unlimited","disklimit":"unlimited","is_locked":0,"suspendtime":null,"email":"","domain":"forjames.mystudenthosting.com","unix_startdate":1414884880,"user":"forjames","plan":"wgueducatorsvega_Student","shell":"/usr/local/cpanel/bin/noshell","maxpop":"unlimited","backup":0,"theme":"x3","owner":"wgueducatorsvega","max_email_per_hour":"unlimited","ipv6":[],"suspendreason":"not suspended","maxlst":"unlimited","suspended":0,"maxsql":"unlimited","maxparked":"*unknown*","partition":"home","maxsub":"unlimited"},{"maxaddons":"*unknown*","ip":"educators.servers.mystudenthosting.com","min_defer_fail_to_trigger_protection":"5","legacy_backup":0,"diskused":"none","maxftp":"unlimited","startdate":"14 Oct 20 07:38","max_defer_fail_percentage":"unlimited","disklimit":"unlimited","is_locked":0,"suspendtime":null,"email":"thunsaker@b12studios.com","domain":"demo.mystudenthosting.com","unix_startdate":1413776312,"user":"demomystudenthos","plan":"default","shell":"/bin/bash","maxpop":"unlimited","backup":0,"theme":"paper_lantern","owner":"wgueducatorsvega","max_email_per_hour":"unlimited","ipv6":[],"suspendreason":"not suspended","maxlst":"unlimited","suspended":0,"maxsql":"unlimited","maxparked":"*unknown*","partition":"home","maxsub":"unlimited"},{"maxaddons":"*unknown*","ip":"educators.servers.mystudenthosting.com","min_defer_fail_to_trigger_protection":"5","legacy_backup":0,"diskused":"none","maxftp":"unlimited","startdate":"14 Nov 01 09:23","max_defer_fail_percentage":"unlimited","disklimit":"unlimited","is_locked":0,"suspendtime":null,"email":"","domain":"bounding.mystudenthosting.com","unix_startdate":1414823001,"user":"bounding","plan":"wgueducatorsvega_Student","shell":"/usr/local/cpanel/bin/noshell","maxpop":"unlimited","backup":0,"theme":"paper_lantern","owner":"wgueducatorsvega","max_email_per_hour":"unlimited","ipv6":[],"suspendreason":"not suspended","maxlst":"unlimited","suspended":0,"maxsql":"unlimited","maxparked":"*unknown*","partition":"home","maxsub":"unlimited"},{"maxaddons":"unlimited","ip":"educators.servers.mystudenthosting.com","min_defer_fail_to_trigger_protection":"5","legacy_backup":0,"diskused":"none","maxftp":"unlimited","startdate":"14 Oct 15 00:03","max_defer_fail_percentage":"unlimited","disklimit":"unlimited","is_locked":0,"suspendtime":null,"email":"thunsaker@b12studios.com","domain":"wgu.educators.vegadigitalsolutions.com","unix_startdate":1413316989,"user":"wgueducatorsvega","plan":"default","shell":"/bin/bash","maxpop":"unlimited","backup":0,"theme":"x3","owner":"wgueducatorsvega","max_email_per_hour":"unlimited","ipv6":[],"suspendreason":"not suspended","maxlst":"unlimited","suspended":0,"maxsql":"unlimited","maxparked":"unlimited","partition":"home","maxsub":"unlimited"},{"maxaddons":"*unknown*","ip":"educators.servers.mystudenthosting.com","min_defer_fail_to_trigger_protection":"5","legacy_backup":0,"diskused":"none","maxftp":"unlimited","startdate":"14 Nov 01 09:17","max_defer_fail_percentage":"unlimited","disklimit":"unlimited","is_locked":0,"suspendtime":null,"email":"","domain":"newstudent.mystudenthosting.com","unix_startdate":1414822644,"user":"newstudent","plan":"wgueducatorsvega_Student","shell":"/usr/local/cpanel/bin/noshell","maxpop":"unlimited","backup":0,"theme":"x3","owner":"wgueducatorsvega","max_email_per_hour":"unlimited","ipv6":[],"suspendreason":"not suspended","maxlst":"unlimited","suspended":0,"maxsql":"unlimited","maxparked":"*unknown*","partition":"home","maxsub":"unlimited"}]},"metadata":{"version":1,"reason":"OK","result":1,"command":"listaccts"}}';
            $return = json_decode($return,true);
            if ($return['metadata']['result'] == "1") {
                return( Array('result' => '1', 'reason' => 'Success', 'data' => $return['data']['acct']));
            }
        }

        public function CreateAccount($data)
        {
          $hash = "40bfdc97ca48d8f81004c69eeadfbc65
1c141ed13d47745eb3122757306095d8
5afe9beecb2442be3bbac304a2ff3b1a
f82ad987210c0f846b5a8f32c58fe708
e59eea6d9743a3afe84e1f56afc8ec14
c62fd27edab6a4f83976fc84b4bebb1d
305d9456326dc8344c94075d8b6a8f60
2997f6eed74bd9c15efaf50e4fb09088
afbcc1f34c6df9101775ff0ac041f64b
642b65216bf6fff99fedc27754fa9e06
ab365194cdffb05e67660ee7fe0fcfa4
067c86ea8d7a4d64a6613097861dba82
e45700c529f98f21c4f498d4d6089e11
f1c1e6f42b81f3af3a070dfe32ce6630
42efcb53949349860e6a4ee4d9c11548
0e24c3b4c01c67f4db1523c8b08fb751
03340e71ccb77257e544a1b397c7071f
90935ac894a35e74e5ab4c18119e4a96
fa05339e43cc42489a0295c2d89fa358
4b485dc8e24a528d98000b41d96d5033
2b92824dc4ca36e9de4750855462692c
dc5eedb70df1e867de528f8c98a50a35
271b52fff778640815cbc186026acc71
25d9b4010abd2739a344329815295ee4
324c610091197e7534886be5e9d53f06
622eb7a2f767fca1eca6e7ee534ee10d
e55feef4fa2a6dda95e7107d454723f7
d8189c715919979979b71f7b697c7d61
b2f9c74b7b5ba3b6b6ec1958ec947b7d";
          $host = 'educators.servers.mystudenthosting.com';
          $user = 'wgueducatorsvega';
          $query = "https://educators.servers.mystudenthosting.com:2087/json-api/createacct?username=".$data['AccountName']."&domain=".$data['AccountName'].".mystudenthosting.com&pkgname=wgueducatorsvega_Student&password=randon";
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);

          $header[0] = "Authorization: WHM $user:" . preg_replace("'(\r|\n|\s)'","",$hash);
          curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
          curl_setopt($curl, CURLOPT_URL, $query);

          $result = curl_exec($curl);          

          $resultobj = json_decode($result,true);
          $resultobj = $resultobj['result'][0];

          if ( !$resultobj ) {
	     $return = array( "result" => "0", "reason" => "Unreadable API Response" );
             return($return);
          }

	  if ( $resultobj['status'] != '1' ) {
            $return = array( "result" => "0", "reason"	=> $resultobj['statusmsg'] );
            return($return);
	  }

          curl_close($curl);
          $return = array( "result" => "1", "reason"	=> "Successful Creation" );
          return($return);
        }

        public function CreateFTP($data)
        {
          $hash = "40bfdc97ca48d8f81004c69eeadfbc65
1c141ed13d47745eb3122757306095d8
5afe9beecb2442be3bbac304a2ff3b1a
f82ad987210c0f846b5a8f32c58fe708
e59eea6d9743a3afe84e1f56afc8ec14
c62fd27edab6a4f83976fc84b4bebb1d
305d9456326dc8344c94075d8b6a8f60
2997f6eed74bd9c15efaf50e4fb09088
afbcc1f34c6df9101775ff0ac041f64b
642b65216bf6fff99fedc27754fa9e06
ab365194cdffb05e67660ee7fe0fcfa4
067c86ea8d7a4d64a6613097861dba82
e45700c529f98f21c4f498d4d6089e11
f1c1e6f42b81f3af3a070dfe32ce6630
42efcb53949349860e6a4ee4d9c11548
0e24c3b4c01c67f4db1523c8b08fb751
03340e71ccb77257e544a1b397c7071f
90935ac894a35e74e5ab4c18119e4a96
fa05339e43cc42489a0295c2d89fa358
4b485dc8e24a528d98000b41d96d5033
2b92824dc4ca36e9de4750855462692c
dc5eedb70df1e867de528f8c98a50a35
271b52fff778640815cbc186026acc71
25d9b4010abd2739a344329815295ee4
324c610091197e7534886be5e9d53f06
622eb7a2f767fca1eca6e7ee534ee10d
e55feef4fa2a6dda95e7107d454723f7
d8189c715919979979b71f7b697c7d61
b2f9c74b7b5ba3b6b6ec1958ec947b7d";
          $host = 'educators.servers.mystudenthosting.com';
          $user = 'wgueducatorsvega';
          $query = "https://educators.servers.mystudenthosting.com:2083/json-api/cpanel?username=".$data['AccountName']."&cpanel_jsonapi_module=FTP&cpanel_jsonapi_func=addftp&cpanel_jsonapi_apiversion=2&user=wgu&pass=random&homedir=public_html";
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);

          $header[0] = "Authorization: WHM $user:" . preg_replace("'(\r|\n|\s)'","",$hash);
          curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
          curl_setopt($curl, CURLOPT_URL, $query);

          $result = curl_exec($curl);
          
          $resultobj = json_decode($result,true);
          $resultobj = $resultobj['result'];

          if ( !$resultobj ) {
	     $return = array( "result" => "0", "reason" => "Unreadable API Response" );
             return($return);
          }

	  if ( $resultobj['status'] != '1' ) {
            $return = array( "result" => "0", "reason"	=> $resultobj['statusmsg'] );
            return($return);
	  }

          curl_close($curl);
          $return = array( "result" => "1", "reason"	=> "Successful Creation" );
          return($return);
        }

        public function passFTP($data, $cpftp)
        {
          $hash = "40bfdc97ca48d8f81004c69eeadfbc65
1c141ed13d47745eb3122757306095d8
5afe9beecb2442be3bbac304a2ff3b1a
f82ad987210c0f846b5a8f32c58fe708
e59eea6d9743a3afe84e1f56afc8ec14
c62fd27edab6a4f83976fc84b4bebb1d
305d9456326dc8344c94075d8b6a8f60
2997f6eed74bd9c15efaf50e4fb09088
afbcc1f34c6df9101775ff0ac041f64b
642b65216bf6fff99fedc27754fa9e06
ab365194cdffb05e67660ee7fe0fcfa4
067c86ea8d7a4d64a6613097861dba82
e45700c529f98f21c4f498d4d6089e11
f1c1e6f42b81f3af3a070dfe32ce6630
42efcb53949349860e6a4ee4d9c11548
0e24c3b4c01c67f4db1523c8b08fb751
03340e71ccb77257e544a1b397c7071f
90935ac894a35e74e5ab4c18119e4a96
fa05339e43cc42489a0295c2d89fa358
4b485dc8e24a528d98000b41d96d5033
2b92824dc4ca36e9de4750855462692c
dc5eedb70df1e867de528f8c98a50a35
271b52fff778640815cbc186026acc71
25d9b4010abd2739a344329815295ee4
324c610091197e7534886be5e9d53f06
622eb7a2f767fca1eca6e7ee534ee10d
e55feef4fa2a6dda95e7107d454723f7
d8189c715919979979b71f7b697c7d61
b2f9c74b7b5ba3b6b6ec1958ec947b7d";
          $host = 'educators.servers.mystudenthosting.com';
          $user = 'wgueducatorsvega';
          $query = "https://educators.servers.mystudenthosting.com:2087/json-api/passwd?api.version=1&user=".$data['AccountName']."&password=".$cpftp."&db_pass_update=1&digestauth=1";
          $curl = curl_init();
          curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
          curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
          curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);
 
          $header[0] = "Authorization: WHM $user:" . preg_replace("'(\r|\n|\s)'","",$hash);
          curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
          curl_setopt($curl, CURLOPT_URL, $query);

          $result = curl_exec($curl);
          $resultobj = json_decode($result,true);
          
          $resultobj = $resultobj['metadata'];
          
          if ( !$resultobj ) {
	     $return = array( "result" => "0", "reason" => "Unreadable API Response" );
             return($return);
          }

	  if ( $resultobj['result'] != '1' ) {
            $return = array( "result" => "0", "reason"	=> $resultobj['reason'] );
            return($return);
	  }

          curl_close($curl);
          $return = array( "result" => "1", "reason"	=> "Successful Creation" );
          
          return($return);
        }
        
    }
