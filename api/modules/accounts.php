<?php
    
    class Accounts {
        
        function __construct($parentObj) {
            $this->moduledata = Array(
                                      'version' => '0.01',
                                      'dependencies' => Array('db','jsonOutput','cPanelAPI'),
                                      'routes' => Array(
                                                        'list' => Array(
                                                                        'method' => 'listAccounts',
                                                                        ),
                                                        'create' => Array(
                                                                          'method' => 'createAccount',
                                                                          ),
                                                        'login' => Array(
                                                                         'method' => 'passiveLogin'
                                                                         ),
                                                        'ftp' => Array(
                                                                       'method' => 'ftplogin'
                                                                      ),
                                                        ),
                                      );
            $this->parent = &$parentObj;
            $class = get_class();
            $this->moduledata['class'] = $class;
        }
        
        function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
        {
            $str = '';
            $count = strlen($charset);
            while ($length--) {
                $str .= $charset[mt_rand(0, $count-1)];
            }
            return $str;
        }
        
        public function listAccounts($data) {
            if ( !isset($data['type']) || $data['type'] == "" || $data['type'] == false ) {
                $data['type'] = 'short';
            } elseif ( strtolower($data['type']) != 'short' && strtolower($data['type']) != 'long' ) {
                error_log("Accounts->List_Accounts: Invalid Type for List Function, Setting to default of Short");
                $data['type'] = 'short';
            }
            # Get All Accounts
            $cPanelAPIData = $this->parent->cPanelAPI->listAccounts();
            # Filter output
            $replacearray = Array(
                                  'diskused' => 'disk_used',
                                  'disklimit' => 'disk_max',
                                  'unix_startdate' => 'account_created',
                                  'user' => 'account_name',
                                  'domain' => 'account_primarydomain',
                                  'maxftp' => 'ftp_max',
                                  'maxsql' => 'sql_max',
                                  'suspended' => 'suspended',
                                  );
            $returnOutput = Array();
            foreach ($cPanelAPIData['data'] as $cpdataobj) {
                $newline = Array();
                foreach ($replacearray as $cpanelkey => $newkey ) {
                    if ( !isset($cpdataobj[$cpanelkey]) ) {
                        continue;
                    }
                    $newline[$newkey] = $cpdataobj[$cpanelkey];
                }
                $returnOutput[] = $newline;
            }
            $return = Array('result' => '1', 'reason' => 'Success', 'data' => $returnOutput);
#            print_r($return);
            return($return);
        }
      public function ftplogin($data) {
            if ( !isset($data['type']) || $data['type'] == "" || $data['type'] == false ) {
                $data['type'] = 'short';
            } elseif ( strtolower($data['type']) != 'short' && strtolower($data['type']) != 'long' ) {
                error_log("Accounts->List_Accounts: Invalid Type for List Function, Setting to default of Short");
                $data['type'] = 'short';
            }
            $cpftp = $this->randstring("25");
            
            $cPanelFTPData = $this->parent->cPanelAPI->passFTP($data,$cpftp); 
            if ( $cPanelFTPData['result'] != '1' ) {
              $this->parent->jsonOutput->sendResponse_GenFail($cPanelFTPData['reason']);
              exit();
            }
            
            $return = Array('result' => '1', 'reason' => 'Success', 'data' => $cpftp);
            //var_dump($return);
            $this->parent->jsonOutput->sendResponse_Ok(array('result' => '1', 'reason' => 'Success', 'data' => $cpftp));
            exit();
#            print_r($return);
            //return($return);
        }
        public function createAccount($data) {
            #{"AccountName": "studentName","Token": "tokens"}
            
            if( !isset($data['AccountName']) || $data['AccountName'] == "" || $data['AccountName'] == false)
            {
                $this->parent->jsonOutput->sendResponse_BadRequest("No Request Data Supplied.");
                exit;
            }

            $cPanelAPIData = $this->parent->cPanelAPI->createAccount($data);
            

            if ( $cPanelAPIData['result'] != '1' ) {
              $this->parent->jsonOutput->sendResponse_GenFail($cPanelAPIData['reason']);
              exit();
            }
            

            $pdo = new PDO('mysql:host=localhost;dbname=base', 'sqladmin', 'RandomStuffMatters');
            $qeary = "INSERT INTO Accounts (cPanelAccount, Status, HomePath, PrimaryDomain, UserID) VALUES ('".$data['AccountName']."','1','/home/".$data['AccountName']."','".$data['AccountName'].".mystudenthosting.com', '".$data['UserID']."')";
            $stmt = $pdo->prepare($qeary);
            $stmt->execute();
            if ( $stmt->rowCount() < 1 ) {
                $this->parent->jsonOutput->sendResponse_GenFail("Failed to create Account, Reason: PMS");
                exit();
            }
            if ( $stmt->rowCount() > 1 ) {
                $this->parent->jsonOutput->sendResponse_GenFail("Failed to create Account, Reason: 2+2=5");
                exit();
            }
            
            $this->parent->jsonOutput->sendResponse_Ok(Array('user' => $data['AccountName'], 'server' => 'educators.instance.mystudenthosting.com', 'domain' => $data['AccountName'].".mystudenthosting.com"),"Success");
            exit();
            
        }
        public function passiveLogin($data)
        {
            
            if( !isset($data['AccountName']) || $data['AccountName'] == "" || $data['AccountName'] == false)
            {
                $this->parent->jsonOutput->sendResponse_BadRequest("No Request Data Supplied.");
                exit;
            }
            $pdobase = new PDO('mysql:host=localhost;dbname=base', 'sqladmin', 'RandomStuffMatters');
            $query = "SELECT * FROM Accounts where cPanelAccount='".$data['AccountName']."' or UserID='".$data['AccountName']."';";
	    error_log($query);
            $stmt = $pdobase->prepare($query);
            $stmt->execute();
            $rows = $stmt->fetchAll();
            if ( count($rows) != '1' ) {
                $this->parent->jsonOutput->sendResponse_GenFail("Failed to create passive login. Reason: Supplied information is invalid.");
                exit();
            }
            
            $data['AccountName'] = $rows[0]['cPanelAccount'];
#	    error_log(var_dump($rows));

            #{"cpusername": "USERNAME","domain": "domain"}
            # Setup cPanel Login Hash for PassiveLogin
            $cploginhash = $this->randstring("256");
            
            $pdo = new PDO('mysql:host=localhost;dbname=badhacks', 'badhacks', 'zomgbadhacks');

            # Drop into the DB
            $query = "DELETE FROM userpassthroughlogins where username='".$data['AccountName']."';";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            
            if(!isset($data['AccountType'])) {
              $data['AccountType'] = '';
            }

            $query = "REPLACE INTO userpassthroughlogins (hash,username,usertype) VALUES ('".$cploginhash."','".$data['AccountName']."','".$data['AccountType']."');";
            $stmt = $pdo->prepare($query);
            $stmt->execute();
            if ( $stmt->rowCount() < 1 ) {
                $this->parent->jsonOutput->sendResponse_GenFail("Failed to create passive login. Reason: Oracle");
                exit();
            }
            $this->parent->jsonOutput->sendResponse_Ok(Array('authhash' => $cploginhash,'fullurl' => 'http://panel.mystudenthosting.com/login?hash='.$cploginhash),"Success");
            exit();
        }
        
    }
