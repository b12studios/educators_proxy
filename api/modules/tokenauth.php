<?php
    
    class tokenAuth {
        
        function __construct($parentObj) {
            $this->moduledata = Array(
                                      'version' => '0.01',
                                      'dependencies' => 'db',
                                  );
            $this->parent = &$parentObj;
            $class = get_class();
            $this->moduledata['class'] = $class;
        }
        
        public function validateToken($token,$ip = false) {
            if (!$ip) {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            return(Array('result' => '1', 'reason' => 'Success'));
        }
        
    }