<?PHP

exit();

# Start Session
session_start();

# Create Environment
$ENV['SERVER'] = &$_SERVER;
$ENV['ROUTING'] = Array();

## Determine Routing Information
$ENV['ROUTING']['URI_PREFIX'] = dirname($_SERVER['SCRIPT_NAME']);
$ENV['ROUTING']['URL'] = $_SERVER['REDIRECT_URL'];
$regex = "/^".str_replace("/","\/",$ENV['ROUTING']['URI_PREFIX'])."/i";
$ENV['ROUTING']['ROUTE'] = preg_replace('/^\//','',preg_replace($regex, "", $ENV['ROUTING']['URL']));

## We have a route now, Clean up neccessary Components
if ( $ENV['ROUTING']['URI_PREFIX'] == '/' ) {
    $ENV['ROUTING']['URI_PREFIX'] = '';
}

/* ## Add POST/GET Vars
$ENV['CLIENT_VARS'] = Array();
$ENV['CLIENT_VARS']['ALL'] = Array();

### GET
$ENV['CLIENT_VARS']['GET'] = Array();
foreach($_GET as $key => $value ) {
  $lowerkey = strtolower($key);
  $upperkey = strtoupper($key);
  $ENV['CLIENT_VARS']['GET'][$key] = $value;
  $ENV['CLIENT_VARS']['ALL'][$key] = Array( 'METHOD' => 'GET', 'DATA' => &$ENV['CLIENT_VARS']['GET'][$key] );
  $ENV['CLIENT_VARS']['GET'][$lowerkey] = &$ENV['CLIENT_VARS']['GET'][$key];
  $ENV['CLIENT_VARS']['ALL'][$lowerkey] = Array( 'METHOD' => 'GET', 'DATA' => &$ENV['CLIENT_VARS']['GET'][$key] );
  $ENV['CLIENT_VARS']['GET'][$upperkey] = &$ENV['CLIENT_VARS']['GET'][$key];
  $ENV['CLIENT_VARS']['ALL'][$upperkey] = Array( 'METHOD' => 'GET', 'DATA' => &$ENV['CLIENT_VARS']['GET'][$key] );
}

### POST
$ENV['CLIENT_VARS']['POST'] = Array();
foreach($_POST as $key => $value ) {
  $lowerkey = strtolower($key);
  $upperkey = strtoupper($key);
  $ENV['CLIENT_VARS']['POST'][$key] = $value;
  $ENV['CLIENT_VARS']['ALL'][$key] = Array( 'METHOD' => 'POST', 'DATA' => &$ENV['CLIENT_VARS']['POST'][$key] );
  $ENV['CLIENT_VARS']['POST'][$lowerkey] = &$ENV['CLIENT_VARS']['POST'][$key];
  $ENV['CLIENT_VARS']['ALL'][$lowerkey] = Array( 'METHOD' => 'POST', 'DATA' => &$ENV['CLIENT_VARS']['POST'][$key] );
  $ENV['CLIENT_VARS']['POST'][$upperkey] = &$ENV['CLIENT_VARS']['POST'][$key];
  $ENV['CLIENT_VARS']['ALL'][$upperkey] = Array( 'METHOD' => 'POST', 'DATA' => &$ENV['CLIENT_VARS']['POST'][$key] );
} */

