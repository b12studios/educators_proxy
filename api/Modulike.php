<?php
    
    class Modulike {
        
        protected $conf;
        protected $main;
        private $internal;
        public $modules;
        
        function __construct($path = "", $requiredModules = Array()) {
            if ( $path == "" && is_dir('./modules') ) {
                $this->conf->module_path = './modules';
            } elseif( $path !== "" && is_dir($path) ) {
                $this->conf->module_path = $path;
            } elseif( $path !== "" ) {
                error_log("Requested module directory \"$path\" was not found.");
                return false;
            } else {
                error_log("Default module directory \"./modules\" not found and no module directory specified.");
                return false;
            }
            $this->enumerateModules();
            if (count($this->internal->modules) == 0) {
                error_log("Could not find modules to install, No Available Functionality!");
            }
            # Replacement for database based loading decisions
            $this->internal->module_unmetdeps = array();
            foreach ($this->internal->modules as $class => $data) {
                # Include the module
                include($this->conf->module_path."/".$data['file']);
                # Instantiate Module
                $this->internal->modules[$class]['object'] = new $class(&$this);
                if ( !$this->internal->modules[$class]['object'] ) {
                    error_log("Failed to load module \"$class\" from file \"$file\"");
                }
                # Verify Module Data
                if (!$this->internal->modules[$class]['object']->moduledata) {
                    error_log("Failed to instantiate module setup :| That's not good. Pulling Module Definitions and Object");
                    $this->internal->modules[$class] = "";
                    continue;
                }
                # Setup Version Information
                if (isset($this->internal->modules[$class]['object']->moduledata['version'])) {
                    $this->internal->modules[$class]['version'] = $this->internal->modules[$class]['object']->moduledata['version'];
                }
                # Dependency Stuffs :)
                ## Unset module deps if we set it up
                if ( isset($this->internal->module_unmetdeps[$class])) {
                    unset($this->internal->module_unmetdeps[$class]);
                }
                # Add to dep requirements if the requested module doesn't exist, unfortunately no version requirements at this time.
                if (isset($this->internal->modules[$class]['object']->moduledata['dependencies'])) {
                    if ( $this->internal->modules[$class]['object']->moduledata['dependencies'] && !is_array($this->internal->modules[$class]['object']->moduledata['dependencies']) && $this->internal->modules[$class]['object']->moduledata['dependencies'] != "" ) {
                        $this->internal->modules[$class]['object']->moduledata['dependencies'] = Array( $this->internal->modules[$class]['object']->moduledata['dependencies'] );
                    } else {
                        $this->internal->modules[$class]['object']->moduledata['dependencies'] = Array();
                    }
                    foreach ($this->internal->modules[$class]['object']->moduledata['dependencies'] as $dep) {
                        if (!isset($this->internal->modules[$dep])) {
                            if(!isset($this->internal->module_unmetdeps[$dep])){
                                $this->internal->module_unmetdeps[$dep] = array();
                            }
                            $this->internal->module_unmetdeps[$dep][] = $class;
                        }
                    }
                }
                # Setup easy access method for Object
                $this->$class = &$this->internal->modules[$class]['object'];
            }
            # Setup Module List
            $this->modules_list = array_keys($this->internal->modules);
            # Setup Dependency Lists
            $this->modules_deps = Array("unmet" => &$this->internal->module_unmetdeps);
        }
        
        private function enumerateModules() {
            $filearray = array_diff(scandir($this->conf->module_path), array('..', '.'));
            $this->internal->modules = array();
            foreach ($filearray as $file) {
                $class = $this->getModuleClass($this->conf->module_path."/".$file);
                if( !$class || $class == "") {
                    continue;
                }
                $this->internal->modules[$class] = Array();
                $this->internal->modules[$class]['file'] = $file;
            }
        }
        
        private function getModuleClass($filepath) {
            if ( !is_file($filepath) ) {
                return false;
            }
            $fp = fopen($filepath, 'r');
            $class = $buffer = '';
            $i = 0;
            while (!$class) {
                if (feof($fp)) break;
                
                $buffer .= fread($fp, 512);
                $tokens = token_get_all($buffer);
                
                if (strpos($buffer, '{') === false) continue;
                
                for (;$i<count($tokens);$i++) {
                    if ($tokens[$i][0] === T_CLASS) {
                        for ($j=$i+1;$j<count($tokens);$j++) {
                            if ($tokens[$j] === '{') {
                                $class = $tokens[$i+2][1];
                            }
                        }
                    }
                }
            }
            return $class;
        }
        
    }
    
