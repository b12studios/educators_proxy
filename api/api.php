<?PHP
    
    ##
    # MSHAPI Version v1 - Codename "Shittasticals"
    # The majority of this will likely be rewritten.
    ##
    
    # Require
    require_once('./Modulike.php');
    
    # User Set Vars ;)
    $module_dir = "./modules";
    $module_deps = Array('jsonOutput','tokenAuth');
    
    # Get API
    $modules = new Modulike($module_dir,$module_deps);
    
    
    # Start Session
    session_start();
    
    # Create Routes
    $ROUTES = Array();
    
    # Create Environment
    $ENV['SERVER'] = &$_SERVER;
    $ENV['ROUTING'] = Array();
    
    ## Determine Routing Information
    $ENV['ROUTING']['URI_PREFIX'] = dirname($_SERVER['SCRIPT_NAME']);
    $ENV['ROUTING']['URL'] = @$_SERVER['REDIRECT_URL'];
    $regex = "/^".str_replace("/","\/",$ENV['ROUTING']['URI_PREFIX'])."/i";
    $ENV['ROUTING']['ROUTE'] = preg_replace('/^\//','',preg_replace($regex, "", $ENV['ROUTING']['URL']));
    
    ## We have a route now, Clean up neccessary Components
    if ( $ENV['ROUTING']['URI_PREFIX'] == '/' ) {
        $ENV['ROUTING']['URI_PREFIX'] = '';
    }
    
    # Setup Dynamic Routes
    foreach ($modules->modules_list as $module) {
        if (!isset($modules->{$module}->moduledata['routes'])) {
            continue;
        }
        foreach ( $modules->{$module}->moduledata['routes'] as $moduleroute => $moduleroutedata) {
            if ( !isset($moduleroutedata['method']) ){
                error_log("Module route \"$moduleroute\" for module \"$module\" does not have a method! Cannot add Route.");
                continue;
            }
            $route = strtolower($module."/".$moduleroute);
            $method = $moduleroutedata['method'];
            $ROUTES[$route] = Array(
                'module' => $module,
                'method' => $method,
                'data' => $moduleroutedata,
            );
        }
    }

    # Setup Post Digest
    ## If we don't have any data in the request, Kick it back.
    if ( count($_POST) < 1 ) {
        $modules->jsonOutput->sendResponse_BadRequest("No Request Data Supplied.");
        exit();
    }
    # If we have too much data in the request, Kick it back.
    if ( count($_POST) > 1 ) {
        $modules->jsonOutput->sendResponse_BadRequest("Multiple Request Data Supplied, Only one was expected.");
        exit();
    }
    
    # Check to see if we have a valid token in the data digest
    $postkey = array_keys($_POST);
    $APIPOST = json_decode($_POST[$postkey[0]],true);
    if (!$APIPOST) {
        $modules->jsonOutput->sendResponse_BadRequest("Invalid data object supplied.");
        exit();
    }
    
    # Check if we're logged in
    $tokenauthresult = $modules->tokenAuth->validateToken($APIPOST);
    if ( $tokenauthresult['result'] != '1' ) {
        $modules->jsonOutput->sendResponse_LoginRequired("Invalid Token: ".$tokenauthresult['reason']);
        exit();
    }
    
    # Do we have a valid route?
    ## Check if we have a route
    if ( !isset($ROUTES[$ENV['ROUTING']['ROUTE']]) ) {
        $modules->jsonOutput->sendResponse_NotImplemented();
        exit();
    }
    ## Check if the route method exists
    if ( $ROUTES[$ENV['ROUTING']['ROUTE']]['method'] == "" ) {
        $modules->jsonOutput->sendResponse_APIError("ROUTE METHOD NOT SPECIFIED! SERVER ERROR!");
        exit();
    }
    ## Check if the route method can be called
    if ( !method_exists($ROUTES[$ENV['ROUTING']['ROUTE']]['module'],$ROUTES[$ENV['ROUTING']['ROUTE']]['method']) ) {
        $modules->jsonOutput->sendResponse_APIError("ROUTE METHOD NOT IMPLEMENTED! SERVER ERROR!");
        exit();
    }
    
    # We have a valid route :)

    # Pull in Client Data from Database to PreFill
    $DATA = Array( 'user_id' => '1', 'user_name' => 'imastudent', 'instanceid' => '1', 'instancename' => 'imastudent.mystudenthosting.com' );
    
    try {
        $routereturn = $modules->{$ROUTES[$ENV['ROUTING']['ROUTE']]['module']}->{$ROUTES[$ENV['ROUTING']['ROUTE']]['method']}($APIPOST);
        if ( $routereturn === false ) {
            $modules->jsonOutput->sendResponse_APIError("Route->Module Execution aborted abnormally.");
            exit();
        }
    } catch (Exception $e) {
        $modules->jsonOutput->sendResponse_APIError("ROUTE EXECUTION ERROR: ".var_export($e));
        exit();
    }
