<?PHP

    # Setup Routes    
    $ROUTES = Array();
    
    # Create Environment
    $ENV['SERVER'] = &$_SERVER;
    $ENV['ROUTING'] = Array();
    
    ## Determine Routing Information
    $ENV['ROUTING']['URI_PREFIX'] = dirname($_SERVER['SCRIPT_NAME']);
    $ENV['ROUTING']['URL'] = @$_SERVER['REDIRECT_URL'];
    $regex = "/^".str_replace("/","\/",$ENV['ROUTING']['URI_PREFIX'])."/i";
    $ENV['ROUTING']['ROUTE'] = preg_replace('/^\//','',preg_replace($regex, "", $ENV['ROUTING']['URL']));
    
    ## We have a route now, Clean up neccessary Components
    if ( $ENV['ROUTING']['URI_PREFIX'] == '/' ) {
        $ENV['ROUTING']['URI_PREFIX'] = '';
    }

    switch($ENV['ROUTING']['ROUTE']) {
      case "v1/login":
	include('./login.php');
      break;

      case "v1/createorlogin":
	include('./createorlogin.php');
      break;

      case "v1/create":
	include('./create.php');
      break;

      default:
	header("HTTP/1.0 404 Not Found");
	echo "404 NOT FOUND";
        exit();
      break;
    }
