<?php 

// Load up the Basic LTI Support code
require_once 'ims-blti/blti.php';

// Initialize, all secrets are 'secret', do not set session, and do not redirect
$deetsarray = Array (
	'mysql_server'   => "localhost",
	'mysql_db'       => "lti",
	'mysql_user'     => "sqladmin",
	'mysql_pass'     => "RandomStuffMatters",
	'table'	       => "blti_keys",
);

$context = new BLTI($deetsarray, false, false);

if ( !$context->valid ) {
  error_log("BLTI FAILURE: ".$context->message);
  die("Invalid Request, Contact support for further assistance.");
}

if ( !isset($_POST['user_id']) || $_POST['user_id'] == '' ) {
  die("Invalid request, Not enough information.");
}

function make_login($userid,$type="") {

  $url = "http://api.mystudenthosting.com/accounts/login";

  $jsondata = Array(
    'AccountName' => $_POST['user_id'],
    'AccountType' => $type,
  );

  $postdata = "apidata=".urlencode(json_encode($jsondata));

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch,CURLOPT_POST,1);
  curl_setopt($ch,CURLOPT_POSTFIELDS, $postdata);

  $result = curl_exec($ch);

  curl_close($ch);

  $resultobj = json_decode($result,true);
  if ( !$resultobj ) {
    die("API Error, Please try again with different data.");
  }

  return($resultobj);
}

function create_account($accountname,$usernum) {

  $url = "http://api.mystudenthosting.com/accounts/create";

  $jsondata = Array(
    'AccountName' => $accountname,
    'UserID' => $usernum,
  );

  $postdata = "apidata=".urlencode(json_encode($jsondata));

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch,CURLOPT_URL,$url);
  curl_setopt($ch,CURLOPT_POST,1);
  curl_setopt($ch,CURLOPT_POSTFIELDS, $postdata);

  $result = curl_exec($ch);

  curl_close($ch);

  $resultobj = json_decode($result,true);
  if ( !$resultobj ) {
    print_r($resultobj);
    die("API Error, Please contact support");
  }

  return($resultobj);
}

if(isset($_POST['roles']) && stripos($_POST['roles'],'instructor') !== false ) {
  $type = "instructor";
} else {
  $type = "student";
}

$resultobj = make_login($_POST['user_id'],$type);

if ( $resultobj['result'] != '1' ) {
  if ( $resultobj['reason'] == 'Failed to create passive login. Reason: Supplied information is invalid.' ) {
    // Create the account :)
    if ( !isset($_POST['user_id']) || $_POST['user_id'] == '' || !isset($_POST['lis_person_sourcedid']) || $_POST['lis_person_sourcedid'] == '' ) {
      die("Invalid request, Not enough information.");
    }
  }

  $url = "http://api.mystudenthosting.com/accounts/create";
  $usernameandrealm = explode(":",$_POST['lis_person_sourcedid']);
  $cleanusername = strtolower(preg_replace('/[^A-Za-z0-9]*/','',$usernameandrealm[1]));
  $resultobj = create_account($cleanusername, $_POST['user_id']);

  if ( $resultobj['result'] != '1' ) {
    die("Failed to Create New Account: ".$resultobj['reason']);
  }   

  // We created, Re-login
  $resultobj = make_login($_POST['user_id']);
  if ( $resultobj['result'] != 1 ) {
    die("Second Login post Creation Failed, Will not attempt again.");
  }

}

header('Location: '.$resultobj['data']['fullurl']);

?>
