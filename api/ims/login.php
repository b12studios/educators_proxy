<?php 
// Load up the Basic LTI Support code
require_once 'ims-blti/blti.php';

// Initialize, all secrets are 'secret', do not set session, and do not redirect
$deetsarray = Array (
	'mysql_server'   => "localhost",
	'mysql_db'       => "lti",
	'mysql_user'     => "sqladmin",
	'mysql_pass'     => "RandomStuffMatters",
	'table'	       => "blti_keys",
);

$context = new BLTI($deetsarray, false, false);

if ( !$context->valid ) {
  error_log("BLTI FAILURE: ".$context->message);
  die("Invalid Request, Contact support for further assistance.");
}

if ( !isset($_POST['user_id']) || $_POST['user_id'] == '' ) {
  die("Invalid request, Not enough information.");
}

$url = "http://api.mystudenthosting.com/accounts/login";
$jsondata = Array(
  'AccountName' => $_POST['user_id'],
);

$postdata = "apidata=".urlencode(json_encode($jsondata));

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch,CURLOPT_URL,$url);
curl_setopt($ch,CURLOPT_POST,1);
curl_setopt($ch,CURLOPT_POSTFIELDS, $postdata);

$result = curl_exec($ch);

curl_close($ch);

$resultobj = json_decode($result,true);
if ( !$resultobj ) {
  die("API Error, Please try again with different data.");
}

if ( $resultobj['result'] != '1' ) {
  die("API Invalid Response, Please contact support");
}

header('Location: '.$resultobj['data']['fullurl']);

?>
