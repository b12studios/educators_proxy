;(function() {

	"use strict";

	var topmenu;
	var sidemenu;
	var instances;

	$.getJSON('http://166.70.63.22/menu.php').done(function(results) { 
		topmenu = results.data.menus.topmenu;
		sidemenu = results.data.menus.sidemenu;
		instances = results.data.instances;

		for (var i = sidemenu.length - 1; i >= 0; i--) {
			var route = sidemenu[i].text.replace(/\s+/, '').toLowerCase();
			sidemenu[i].route = route;
			if (sidemenu[i].submenu) {
				var submenu = sidemenu[i].submenu;
				for (var j = submenu.length - 1; j >= 0; j--) {
					sidemenu[i].submenu[j].route = route+"."+submenu[j].url;
				}
			}
		console.log(sidemenu[i]);
		}

		$.each(topmenu, function(a,b) {
			$('body').append("<script type=\"text/x-handlebars\" id=\""+b.text+"\">{{#if url}}<iframe {{bind-attr src='topmenu.url'}} width='100%' height='100%'></iframe>{{else}}<div class='welcome'><h1>{{text}}</h1></div>{{/if}}</script>");
		});

	}).then(function() {
		var App = Ember.Application.create({
		});

		App.Router.map(function() {
			for (var i = 0; i<topmenu.length; i++) {
				this.route(topmenu[i].text, {path: "/"+instances[0].id+"/"+topmenu[i].text.toLowerCase()});
			}
			for (var i = 0; i<sidemenu.length; i++) {
				if (sidemenu[i].submenu) {
					var title = sidemenu[i].route;
					var submenu = sidemenu[i].submenu;
					this.resource(title, function() {
						for (var j = 0; j<submenu.length; j++) {
							if (submenu[j]) {
								this.route(submenu[j].url);
							}
						}
					});
				} else {
					this.route(sidemenu[i]);
				}
			}
		});

		$.each(topmenu, function(a, b) {

			App[b.text+"Route"] = Ember.Route.extend({
				model: function() {
					return b;
				}
			});
		});

		$.each(sidemenu, function(a, b) {
			App[b.text.replace(/\s+/, '')+"Route"] = Ember.Route.extend({
				model: function() {
					return b;
				}
			});
		});
		
		App.ApplicationRoute = Ember.Route.extend({	  
		  model: function() {
		  	return {topmenu: topmenu,
		  		sidemenu: sidemenu, 
		  		instances: instances,
		  		test: "test"};
		  }
		});

		App.ApplicationView = Em.View.extend({
			didInsertElement: function() {
				Ember.run.scheduleOnce('afterRender', jsEffects);
				Ember.run.scheduleOnce('afterRender', closeWindow);
			}
		});

	});

})();

function jsEffects() {
	var menu = document.getElementById('gn-menu');
	var trigger = $('.gn-dropdown-trigger');
	var dropDown = $('.gn-dropdown');
	var sideItem = $('.sideItem');
	var submenu = $('.gn-submenu');

	submenu.slideUp();
	dropDown.hide();

	trigger.mouseenter(function(){
		dropDown.slideDown('ease');
	});
	trigger.mouseleave(function(){
		dropDown.slideUp('ease');
	});
	
	sideItem.click(function(){
		$(this).find('.gn-submenu').slideDown('ease');
	});

	submenu.mouseleave(function(){
		$(this).slideUp('');
	});

	if (menu) {
		new gnMenu(menu);
	}
}

function closeWindow() {
	var signOut = $('#signOut');
	signOut.on('click', function() {
		window.close();
	});
};