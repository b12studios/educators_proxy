<?php
    
// This function needs a selected option
  function sidemenugen($menuarray, $inner = false) {
    foreach ($menuarray as $menuline) {
      echo "  <li><a class=\"";
      if ( !$inner ) {
        echo "gn-icon";
      }
      if ( isset($menuline['icon']) && $menuline['icon'] != '' ) {
        echo " fa-".$menuline['icon'];
      }
      echo "\"";
      if ( isset($menuline['url']) && $menuline['url'] != '' ) {
        echo " href=\"".$menuline['url']."\"";
      }
      echo ">".$menuline['text']."</a>";
      if ( isset($menuline['submenu']) ) {
        echo "\n    <ul class=\"gn-submenu\">";
        echo sidemenugen($menuline['submenu'], true);
        echo "</ul>";
      }
      echo "</li>\n";
    }
  }
    function topmenugen($menuarray){
        foreach ($menuarray as $menuline) {
            echo "<li><a ";
            if ( @$menuline['selected'] == 1 ) {
                 echo "class=\"menuactive\" ";
            }
            echo "href=\"".$menuline['url']."\">";
            if ( isset($menuline['icon']) && $menuline['icon'] != "" ) {
                 echo "<span class=\"fa fa-".$menuline['icon']."\"></span>";
            }
            echo $menuline['text']."</a></li>";
        }
    }
?>
<!DOCTYPE html>
<!-- Here is the array being used:

<?php print_r($templatevar); ?>

-->
<html lang="en" class="no-js">
	<head>
		<meta charset="UTF-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title><?php echo $templatevar['document']['title']; ?></title>
        <link rel="shortcut icon" href="../favicon.ico">
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
        <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="css/student.css" />
		<script src="js/modernizr.custom.js"></script>
        <script src="js/jquery-1.11.1.min.js"></script>
        <script>

        </script>
	</head>
	<body>
		<div class="container">
			<ul id="gn-menu" class="gn-menu-main">
				<li class="gn-trigger">
					<a class="gn-icon gn-icon-menu"><span>Menu</span></a>
					<nav class="gn-menu-wrapper">
						<div class="gn-scroller">
							<ul class="gn-menu">
<?php echo sidemenugen($templatevar['sidemenu']); ?>
							</ul>
						</div><!-- /gn-scroller -->
					</nav>
				</li>
<?php echo topmenugen($templatevar['topmenu']); ?>
<li><a href="#"><?php echo $templatevar['document']['account']; ?>&nbsp;&nbsp;<span class="fa fa-angle-down"></span></a></li>
			</ul>
		</div><!-- /container -->
        <div id="contentbody">
        <?php
            if ( $templatevar['document']['bodycontent_type'] == "iframe" ) {
               echo "<div id=\"iframe\"><iframe src=\"".$templatevar['document']['bodycontent']."\" seamless></iframe></div>";
            } else {
                echo $templatevar['document']['bodycontent'];
            };
        ?>
        </div>
		<script src="js/classie.js"></script>
		<script src="js/gnmenu.js"></script>
		<script>
            function resizeIframe()
            {
                $("#iframe").height( WindowHeight() - getObjHeight(document.getElementById("gn-menu")) );
            }
        
            function WindowHeight()
            {
                var de = document.documentElement;
                return self.innerHeight || (de && de.clientHeight ) || document.body.clientHeight;
            }
            
            function getObjHeight(obj)
            {
                if( obj.offsetWidth )
                {
                    return obj.offsetHeight;
                }
                return obj.clientHeight;
            }
        
            $(document).ready(main);
        
            function main() {
                resizeIframe();
                
            }
        
            $(window).resize( function() {resizeIframe();} );
            new gnMenu( document.getElementById( 'gn-menu' ) );

		</script>
	</body>
</html>
