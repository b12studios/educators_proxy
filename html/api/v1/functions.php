<?php
function req($hash,$user,$query)
{
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST,0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER,0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER,1);

    $header[0] = "Authorization: WHM $user:" . preg_replace("'(\r|\n|\s)'","",$hash);
    curl_setopt($curl,CURLOPT_HTTPHEADER,$header);
    curl_setopt($curl, CURLOPT_URL, $query);

    $result = curl_exec($curl);
    if ($result == false) {
        error_log("curl_exec threw error \"" . curl_error($curl) . "\" for $query");
    }
    curl_close($curl);
    return $result;
}
function cpanelListApi($hash,$user,$module,$function,$cpuser,$version)
{
    $query = "https://166.70.139.29:2087/json-api/cpanel?cpanel_jsonapi_module=$module&cpanel_jsonapi_func=$function&cpanel_jsonapi_user=$cpuser&cpanel_jsonapi_apiversion=$version";
    $results = req($hash,$user,$query);
    return $results;
}

function whmCreateAccount($hash,$user,$cpuser)
{
    $query = "https://166.70.139.29:2087/json-api/createacct?username=".$cpuser."&domain=".$cpuser.".mystudenthosting.com&pkgname=wgueducatorsvega_Student&password=randon";
    $results = req($hash,$user,$query);
    return $results;
}

?>
