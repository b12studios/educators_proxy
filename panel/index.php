<?php

session_start();

# Lets figure out if we're in a subdir
$prefix = dirname($_SERVER['SCRIPT_NAME']);
$url = $_SERVER['REDIRECT_URL'];
$regex = "/^".str_replace("/","\/",$prefix)."/i";
$route = preg_replace('/^\//','',preg_replace($regex, "", $url));
# We have a route now :D
if ( $prefix == '/' ) {
    $prefix = '';
}

unset($regex);
unset($url);

if ( isset($_SESSION['cpdomain']) ) {
  $account = $_SESSION['cpdomain'];
} else {
  $account = false;
}

$topmenu = Array (
    'home' => Array(
          'icon' => 'house',
          'text' => 'Home',
          'url' => 'http://panel.mystudenthosting.com/home',
          ),
    'files' => Array(
          'icon' => 'files-o',
          'text' => 'Files',
          'url' => 'http://panel.mystudenthosting.com/files',
          ),
    'mysql' => Array(
          'icon' => 'database',
          'text' => 'MySQL',
          'url' => 'http://panel.mystudenthosting.com/mysql',
          ),
    'mysite' => Array(
          'icon' => 'globe',
          'text' => 'My Site',
          'url' => 'http://panel.mystudenthosting.com/mysite',
          ),
    'ftp' => Array(
          'icon' => 'folder',
          'text' => 'FTP',
          'url' => 'http://panel.mystudenthosting.com/ftp',
          ),
    );

$sidemenu = Array(
                  Array(
                        'icon' => 'power-off',
                        'text' => 'Session',
                        'submenu' => Array(
                                           Array( 'url' => 'http://panel.mystudenthosting.com/logout', 'text' => 'Log Out' ),
                                           ),
                        ),
/*                  Array(
                        'icon' => 'flash',
                        'text' => 'Hosting Management',
                        'submenu' => Array(
                                           Array( 'url' => 'http://panel.mystudenthosting.com/manage/ftpaccounts', 'text' => 'FTP Accounts' ),
                                           Array( 'url' => 'http://panel.mystudenthosting.com/manage/databases', 'text' => 'Databases' ),
                                           Array( 'url' => 'http://panel.mystudenthosting.com/manage/phpconf', 'text' => 'PHP Configuration' ),
                                           ),
                        ),
                  Array(
                        'icon' => 'gears',
                        'text' => 'Preferences',
                        'submenu' => Array(
                                           Array( 'url' => 'http://panel.mystudenthosting.com/prefs/general', 'text' => 'General' ),
                                           Array( 'url' => 'http://panel.mystudenthosting.com/prefs/modules/files', 'text' => 'Files' ),
                                           Array( 'url' => 'http://panel.mystudenthosting.com/prefs/modules/mysql', 'text' => 'MySQL' ),
                                           ),
                        ),
                  Array(
                        'icon' => 'user',
                        'text' => 'My Account',
                        'submenu' => Array(
                                           Array( 'url' => 'http://panel.mystudenthosting.com/myaccount', 'text' => 'Profile' ),
                                           ),
                        ),
                  Array(
                        'icon' => 'question-circle',
                        'text' => 'Help',
                        'submenu' => Array(
                                           Array( 'url' => 'http://panel.mystudenthosting.com/help/interface', 'text' => 'Interface' ),
                                           Array( 'url' => 'http://panel.mystudenthosting.com/help/modules', 'text' => 'Modules' ),
                                           Array( 'url' => 'http://panel.mystudenthosting.com/help/hostingmgmt', 'text' => 'Hosting Management' ),
                                           Array( 'url' => 'http://panel.mystudenthosting.com/help/myaccount', 'text' => 'My Account' ),
                                           Array( 'url' => 'http://panel.mystudenthosting.com/help/preferences', 'text' => 'Preferences' ),
                                           ),
                        ), */
                  );

if ( $route !== "login" && $_SESSION['login'] !== true ) {
  $route = "";
}

switch ($route) {
    case "files":
        $topmenu['files']['selected'] = 1;
        $url = 'http://educators.instance.mystudenthosting.com/cpsess'.$_SESSION['cpsession'].'/frontend/paper_lantern/filemanager/index.html';
        $templatevar = array(
                             'document' => Array(
                                                 'title' => 'My Student Hosting - MySQL',
                                                 'bodycontent_type' => 'iframe',
                                                 'bodycontent' => $url,
                                                 'account' => $account,
                                                 ),
                             'topmenu'  => $topmenu,
                             'sidemenu' => $sidemenu,

                             );
            include('./template.php');
        break;
    case "mysql":
        $topmenu['mysql']['selected'] = 1;
        $url = 'http://educators.instance.mystudenthosting.com/cpsess'.$_SESSION['cpsession'].'/3rdparty/phpMyAdmin/index.php';
        $templatevar = array(
                             'document' => Array(
                                                 'title' => 'My Student Hosting - MySQL',
                                                 'bodycontent_type' => 'iframe',
                                                 'bodycontent' => $url,
                                                 'account' => $account,
                                                 ),
                             'topmenu'  => $topmenu,
                             'sidemenu' => $sidemenu,

                             );
            include('./template.php');
        break;
    case "mysite":
        $topmenu['mysite']['selected'] = 1;
        $url = 'http://'.$account;
        $templatevar = array(
                             'document' => Array(
                                                 'title' => 'My Student Hosting - My Site',
                                                 'bodycontent_type' => 'iframe',
                                                 'bodycontent' => $url,
                                                 'account' => $account,
                                                 ),
                             'topmenu'  => $topmenu,
                             'sidemenu' => $sidemenu,

                             );
        include('./template.php');
        break;
    case "ftp":
        $topmenu['ftp']['selected'] = 1;
        $url = 'http://'.$account;
        $user = explode('.', $account);
        $templatevar = array(
                             'document' => Array(
                                                 'title' => 'My Student Hosting - FTP',
                                                 'bodycontent_type' => 'string',
                                                 'bodycontent' => '<div class="welcomediv">
                                                 <h1>Welcome to the FTP Portal!<span>You can login to FTP with:</span></h1>
                                                 <p> URL: '.$account.'
                                                 <br>Username: '.$user[0].' 
                                                 <br>Password: '.$_SESSION['cpftppasswd'].'</p>
                                                 </div>',
                                                 'account' => $account,
                                                 ),
                             'topmenu'  => $topmenu,
                             'sidemenu' => $sidemenu,

                             );
        include('./template.php');
        break;
    case "home":
        $topmenu['home']['selected'] = 1;
        if(isset($_SESSION['instructor']) && $_SESSION['instructor'] ) {
          $wgutype = "Instructor";
          $wguaddtlinstr = " or select a student hosting package";
        } else {
          $wgutype = "Student";
          $wguaddtlinstr = "";
        }
        $templatevar = array(

                             'document' => Array(
                                                 'title' => 'My Student Hosting - Welcome!',
                                                 'bodycontent_type' => 'string',
                                                 'bodycontent' => '<div class="welcomediv">
                                                 <h1>Welcome WGU '.$wgutype.'!<span>Use the menus above to select an action'.$wguaddtlinstr.'.</span></h1>
                                                 </div>',
                                                 'account' => $account,
                                                 ),
                             'topmenu'  => $topmenu,
                             'sidemenu' => $sidemenu,

                             );
        include('./template.php');
        break;
    case "logout":
        session_destroy();
        echo "Session Destroyed.";
        break;
    case "login":
	$pdo = new PDO('mysql:host=localhost;dbname=badhacks', 'badhacks', 'zomgbadhacks');
        $query = "SELECT * from userpassthroughlogins where hash='".$_GET['hash']."'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $mysqldata = $stmt->fetchAll();

        if (count($mysqldata) != '1') {
          error_log("[Panel] Could not find hash for login.");
          die("[Panel] Login Error, OTL Could not be validated.");
          exit;
        }
        

        $mysqldata = $mysqldata[0];

        $query = "DELETE from userpassthroughlogins where hash='".$_GET['hash']."'";
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        # URL Login Validation and Requirement
        if ($mysqldata['usertype'] == 'instructor' ) {
	        $_SESSION['instructor'] = 1;
        }
        $_SESSION['cpusername'] = $mysqldata['username'];
        $_SESSION['cpdomain'] = $mysqldata['username'].'.mystudenthosting.com';
        // set URL and other appropriate options
        $url = "http://api.mystudenthosting.com/accounts/ftp";
        $mtvars = 'data={"AccountName": "'.$_SESSION['cpusername'].'"}';
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_POST, 1);
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $mtvars);
        curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt( $ch, CURLOPT_HEADER, 0);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1);
        // grab URL and pass it to the browser
        $passwdJson = curl_exec($ch);
        $passwdArray = json_decode($passwdJson,true);
        print_r($passwd);

        $_SESSION['cpftppasswd'] = $passwdArray['data']['data'];
        # Setup cPanel Login Hash for PassiveLogin
        $cploginhash = randstring("256");

	# Drop into the DB
	$pdo = new PDO('mysql:host=localhost;dbname=badhacks', 'badhacks', 'zomgbadhacks');
        $query = "DELETE FROM cppassthroughlogins where username='".$_SESSION['cpusername']."';";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $query = "INSERT INTO cppassthroughlogins (hash,username) VALUES ('".$cploginhash."','".$_SESSION['cpusername']."');";
        $stmt = $pdo->prepare($query);
        $stmt->execute();

        # Set Logged in
        $_SESSION['login'] = true;

        header('Location: http://educators.instance.mystudenthosting.com/passivelogin/?hash='.$cploginhash);
        break;
    case "cpreturn":
	if (!isset($_GET['session']) || $_GET['session'] == '' ) {
          error_log("[Panel] CPReturn Failure, Session not present");
          die("[Panel] Application Error, Return from remote Application was not valid.");
        }

        # Init the stuff to actually CPLogin
	$_SESSION['cpsession'] = $_GET['session'];

  	header('Location: '.'http://panel.mystudenthosting.com/home');
        break;
    default:
        if ( isset($_SESSION['login']) && $_SESSION['login'] == "true" ) {
            header('Location: '.'http://panel.mystudenthosting.com/home');
        } else {
	    die("Invalid Session, Please re-login");
        }
        exit;
}

function randString($length, $charset='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789')
{
    $str = '';
    $count = strlen($charset);
    while ($length--) {
        $str .= $charset[mt_rand(0, $count-1)];
    }
    return $str;
}
